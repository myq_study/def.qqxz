package com.def.monitor.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @ClassName MonitorCoreApplication
 * @Description 监控应用启动
 * @Date 2022/10/27
 * @Author myq
 */
@EnableScheduling
@Slf4j
@SpringBootApplication
public class MonitorCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorCoreApplication.class,args);
        log.info("***************************监控系统资源进程启动完成***************************");
    }
}
