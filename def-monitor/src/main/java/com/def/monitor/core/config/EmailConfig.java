package com.def.monitor.core.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;


@Configuration
@ConfigurationProperties(prefix = "email")
@Getter
@Setter
public class EmailConfig {

    // 发送账号
    private String emailAccount;
    // 发送密码
    private String emailPassword;
    // 邮件发送服务器地址
    private String emailSmtpHost;
    // 接收者账号
    private String receiveEmailAccount;

    public Properties properties() {
        return new EmailProperties().get();
    }

    private class EmailProperties extends EmailConfig {

        public Properties get() {
            //参数配置
            Properties props = new Properties();
            //使用的协议（JavaMail规范要求）
            props.setProperty("mail.transport.protocol", "smtp");
            //发件人的邮箱的 SMTP 服务器地址
            props.setProperty("mail.smtp.host", emailSmtpHost);
            //需要请求认证
            props.setProperty("mail.smtp.auth", "true");
            return props;
        }
    }
}
