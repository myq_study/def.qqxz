package com.def.monitor.core.email;


import com.def.monitor.core.config.EmailConfig;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @ClassName AbstractEmail
 * @Description
 * @Date 2022/10/27
 * @Author myq
 */
public abstract class AbstractEmail<T> implements Email<T> {

    private EmailConfig emailConfig;

    private Properties props;

    private Template.ReferencePipeline<MimeMessage> pipeline;

    public AbstractEmail(EmailConfig emailConfig) {
        this.emailConfig = emailConfig;
        // 构建props 子类负责初始化
        this.props = emailConfig.properties();
        this.pipeline = build();
    }


    /**
     * @Description: 构建消息生成规则（由子类初始化）
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2814:39
     */
    public Template.ReferencePipeline<MimeMessage> build() {
        return new Template.ReferencePipeline<MimeMessage>() {

            @Override
            public MimeMessage get(Email<MimeMessage> email) {
                return email.get();
            }
        };
    }


    /**
     * @Description: 发送逻辑
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2717:13
     */
    public void handle(Email email) {
        Transport transport = null;
        try {
            //2.根据配置创建会话对象, 用于和邮件服务器交互
            Session session = Session.getInstance(props);
            //设置为debug模式,可以查看详细的发送log
            //session.setDebug(true);
            //3.创建一封邮件
            MimeMessage mimeMessage = pipeline.get(email);
            //4.根据 Session获取邮件传输对象
            transport = session.getTransport();
            // 5.使用邮箱账号和密码连接邮件服务器, 这里认证的邮箱必须与 message中的发件人邮箱一致,否则报错
            //
            //    PS_01: 成败的判断关键在此一句, 如果连接服务器失败, 都会在控制台输出相应失败原因的 log,
            //           仔细查看失败原因, 有些邮箱服务器会返回错误码或查看错误类型的链接, 根据给出的错误
            //           类型到对应邮件服务器的帮助网站上查看具体失败原因。
            //
            //    PS_02: 连接失败的原因通常为以下几点, 仔细检查代码:
            //           (1)邮箱没有开启 SMTP 服务;
            //           (2)邮箱密码错误, 例如某些邮箱开启了独立密码;
            //           (3)邮箱服务器要求必须要使用 SSL 安全连接;
            //           (4)请求过于频繁或其他原因, 被邮件服务器拒绝服务;
            //           (5)如果以上几点都确定无误, 到邮件服务器网站查找帮助。
            //
            //PS_03:仔细看log,认真看log,看懂log,错误原因都在log已说明。
            transport.connect(emailConfig.getEmailAccount(), emailConfig.getEmailPassword());
            //6.发送邮件,发到所有的收件地址,message.getAllRecipients()获取到的是在创建邮件对象时添加的所有收件人, 抄送人, 密送人
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        } catch (MessagingException e) {
            e.printStackTrace();
        } finally {
            // 7. 关闭连接
            try {
                if (transport != null)
                    transport.close();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }


}

