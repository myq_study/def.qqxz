package com.def.monitor.core.email;

import java.util.function.Supplier;

/**
 * @ClassName Email
 * @Description 接口
 * @Date 2022/10/28
 * @Author myq
 */
public interface Email<T> extends Supplier<T> {

    /**
     * @Description: 返回一个T 请实现此接口
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/289:39
     */
    @Override
    default T get() {
        System.out.println("输出模板，返回一个MimeMessage");
        return null;
    }


    public void send();


    abstract class Provider<T> implements Email<T> {


    }

}


