package com.def.monitor.core.email;

/**
 * @ClassName Template
 * @Description 抽象类以帮助子类实现@{Email}的接口
 * @Date 2022/10/28
 * @Author myq
 */
abstract class Template<T> {

    public abstract T get(Email<T> email);

    static abstract class ReferencePipeline<T> extends Template<T> {

    }
}
