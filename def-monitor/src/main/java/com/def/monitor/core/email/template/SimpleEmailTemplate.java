package com.def.monitor.core.email.template;

import com.def.monitor.core.config.EmailConfig;
import com.def.monitor.core.email.AbstractEmail;
import com.def.monitor.core.pojo.SystemInfo;
import lombok.extern.slf4j.Slf4j;

import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;

/**
 * @ClassName SimpleEmailTemplate
 * @Description 简单的模板、示例模板
 * @Date 2022/10/27
 * @Author myq
 */
@Slf4j
public class SimpleEmailTemplate<T> extends AbstractEmail<T> {

    private Session session;

    private String sendMail;

    private String receiveMail;

    private SystemInfo systemInfo;

    public SimpleEmailTemplate(EmailConfig emailConfig, SystemInfo systemInfo) {
        super(emailConfig);
        this.systemInfo = systemInfo;
        this.session = Session.getInstance(emailConfig.properties());
        this.sendMail = emailConfig.getEmailAccount();
        this.receiveMail = emailConfig.getReceiveEmailAccount();

    }

    /**
     * 创建一封只包含文本的简单邮件
     *
     * @return
     */
    @Override
    public T get() {
        MimeMessage message = null;
        try {
            // 1.创建一封邮件
            message = new MimeMessage(session);
            // 2.From:发件人（昵称有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改昵称）
            message.setFrom(new InternetAddress(sendMail, "人工智能", "UTF-8"));
            // 3.To:收件人（可以增加多个收件人、抄送、密送）
            message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "系统运维人员", "UTF-8"));
            // 4.Subject: 邮件主题（标题有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改标题）
            message.setSubject("资源预警", "UTF-8");
            // 5.Content: 邮件正文（可以使用html标签）（内容有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改发送内容）
            message.setContent(systemInfo.toString(), "text/html;charset=UTF-8");
            // 6.设置发件时间
            message.setSentDate(new Date());
            // 7.保存设置
            message.saveChanges();
        } catch (Exception e) {
            log.error("创建一封简单的邮件异常：{}", e.toString());
            throw new RuntimeException("构建消息异常");
        }
        return (T) message;
    }

    /**
     * @Description: 发送接口
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2814:13
     */
    @Override
    public void send() {
        super.handle(this);
    }
}
